import {
  getHtml,
  getTotalPages,
  scrapeSingleListPage,
  scrapeTruckItem,
} from './services/base-service';
import {
  addItems,
  getAllItems,
  getTotalAdsCount,
  setTotalAdsCnt,
  updateItem,
} from './services/db-service';

const baseUrl = 'www.otomoto.pl';
const initialPath =
  '/ciezarowe/uzytkowe/mercedes-benz/od-2014/q-actros?search%5Bfilter_enum_damaged%5D=0&search%5Border%5D=created_at%3Adesc';

export const targetTruckDetailKeys: any = {
  productionDate: ['Year of production', 'Rok produkcji'],
  power: ['Power', 'Moc'],
  registrationDate: ['First registration', 'Pierwsza rejestracja'],
  mileage: ['Course', 'Przebieg'],
};

const getNextPageUrl = async () => {
  let html = await getHtml(baseUrl, initialPath);
  const pageCnt = getTotalPages('.ooa-g4wbjr.ekxs86z0', html);

  const pattern = 'main article';
  const subpattern = [
    {
      name: 'title',
      type: 'text',
      filterBy: 'div h2 a',
    },
    {
      name: 'href',
      type: 'attr',
      filterBy: 'div h2 a',
      attrName: 'href',
    },
    {
      name: 'price',
      type: 'text',
      filterBy: 'div.e1b25f6f9.ooa-1w7uott-Text.eu5v0x0 span',
    },
    {
      name: 'itemId',
      type: 'attr',
      filterBy: undefined,
      attrName: 'id',
    },
  ];

  let ads = await scrapeSingleListPage(pattern, subpattern, html);
  setTotalAdsCnt(ads.length);
  addItems(ads);

  for (let i = 2; i <= pageCnt; i++) {
    html = await getHtml(baseUrl, `${initialPath}&page=${i}`);
    ads = await scrapeSingleListPage(pattern, subpattern, html);
    addItems(ads);
  }
};

const scrapeAllTruckItem = async () => {
  for (let item of getAllItems()) {
    const result = await scrapeTruckItem(
      'main div.parametersArea div.offer-params.with-vin ul.offer-params__list li.offer-params__item',
      targetTruckDetailKeys,
      baseUrl,
      item
    );
    updateItem(item.id, result);
  }
  getAllItems();
  console.log('All detail page scrape successful');
};

getNextPageUrl()
  .then(getAllItems)
  .then(getTotalAdsCount)
  .then(scrapeAllTruckItem)
  .then((data) => {
    console.log('Successlly scrapped all pages');
  })
  .catch((error) => console.log(error));
