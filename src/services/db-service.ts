import { Item } from '../models/item';

let items: Item[] = [];
let totalInitPageAdCnt = 0;

export const setTotalAdsCnt = (totalAds: number) => {
  totalInitPageAdCnt = totalAds;
};
export const getTotalAdsCount = (): number => {
  console.log('total ads', totalInitPageAdCnt);
  return totalInitPageAdCnt;
};

export const addItems = (ads: Item[]) => {
  let lastId = items.length === 0 ? 0 : items[items.length - 1].id;

  for (let i = 0; i < ads.length; i++) {
    ads[i].id = ads[i].id + lastId;
  }
  items = items.concat(ads);
};

export const getAllItems = (): Item[] => {
  console.log('items', items);
  return items;
};

export const updateItem = (id: number, truckDetails: any) => {
  const item = items[id - 1];
  const modifiedItem = { ...item, ...truckDetails };
  items[id - 1] = modifiedItem;
};
